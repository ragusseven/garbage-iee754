using System.Globalization;
using NUnit.Framework;

namespace c_sharp_impl;

public class Tests
{
    [TestCase(-16000)]
    [TestCase(-15000)]
    [TestCase(-10000)]
    [TestCase(-512)]
    [TestCase(-127)]
    [TestCase(-5)]
    [TestCase(-4)]
    [TestCase(-1)]
    [TestCase(0)]
    [TestCase(1)]
    [TestCase(4)]
    [TestCase(5)]
    [TestCase(127)]
    [TestCase(512)]
    [TestCase(1000)]
    [TestCase(15000)]
    [TestCase(16000)]
    public void IntToIeee754FloatTest(short input)
    {
        var ieee = Ieee754Number.GetFromInt16(input);
        Assert.That(ieee.ToString() == ((float)input).ToString("F1"), ieee.ToString);
    }

    //
    [TestCase("0100001011110110", "0011111011111010", 123.123f)]
    public void IntToIeee754FloatTest(string a, string b, float input)
    {
        var ieee = new Ieee754Number()
        {
            _a = Convert.ToUInt16(a, 2),
            _b = Convert.ToUInt16(b, 2)
        };
        Assert.That(float.Parse(ieee.ToString()).ToString("F1") == ((float)input).ToString("F1"),
            float.Parse(ieee.ToString()).ToString("F1")
            + "expected to be" + ((float)input).ToString("F1"));
    }

    [TestCase(-16000)]
    [TestCase(-15000)]
    [TestCase(-10000)]
    [TestCase(-512)]
    [TestCase(-127)]
    [TestCase(-5)]
    [TestCase(-4)]
    [TestCase(-1)]
    [TestCase(0)]
    [TestCase(1)]
    [TestCase(4)]
    [TestCase(5)]
    [TestCase(127)]
    [TestCase(512)]
    [TestCase(10000)]
    [TestCase(15000)]
    [TestCase(16000)]
    public void IntTestToFromParts(short input)
    {
        var ieee = Ieee754Number.GetFromInt16(input);
        var parts = ieee.ToParts();
        Assert.That(
            Ieee754Number.FromParts(parts.sign, parts.integer, parts.drob).ToString() == ((float)input).ToString("F1"),
            ieee.ToString() + " " + Ieee754Number.FromParts(parts.sign, parts.integer, parts.drob).ToString());
    }

    [TestCase(-16000)]
    [TestCase(-15000)]
    [TestCase(-10000)]
    [TestCase(-512)]
    [TestCase(-127)]
    [TestCase(-5)]
    [TestCase(-4)]
    [TestCase(-1)]
    [TestCase(0)]
    [TestCase(1)]
    [TestCase(4)]
    [TestCase(5)]
    [TestCase(127)]
    [TestCase(512)]
    [TestCase(10000)]
    [TestCase(15000)]
    [TestCase(16000)]
    public void PlusTests(short input)
    {
        var ieee = Ieee754Number.GetFromInt16(input);
        var parts = ieee.ToParts();
        Assert.That(ieee.Plus(ieee).ToString() == ((float)input + input).ToString("F1"),
            ieee.Plus(ieee).ToString() + " " + input);
    }

    [TestCase(10, -20)]
    [TestCase(10, -30)]
    [TestCase(-10, 20)]
    [TestCase(-20, -10)]
    [TestCase(-20, 10)]
    public void ComplPlusTests(short inputa, short inputb)
    {
        var ieee1 = Ieee754Number.GetFromInt16(inputa);
        var ieee2 = Ieee754Number.GetFromInt16(inputb);
        Assert.That(ieee1.Plus(ieee2).ToString() == ((float)inputa + inputb).ToString("F1"),
            ieee1.Plus(ieee2).ToString() + " " + (inputa + inputb));
    }

    [TestCase((ushort)10, (ushort)13088, 10.2f)]
    [TestCase((ushort)0, (ushort)19648, 0.3f)]
    public void ComplFloatPlusTests(ushort inputa, ushort inputb, float number)
    {
        var ieee1 = Ieee754Number.FromParts(0, inputa, inputb);
        var ieee2 = Ieee754Number.FromParts(0, inputa, inputb);
        RightShift(-2, 1);
        var str = PrintBinaInt(Convert.ToInt16("1000111100110000", 2));
        Assert.That(ieee1.Plus(ieee2).ToFloat() == ((float)number + number).ToString("F1"),
            ieee1.Plus(ieee2).ToString() + " " + (number + number));
    }

    private short LeftShift(short num, short n)
    {
        var i = 0;

        while (i < n)
        {
            num = (short)(num * 2);
            var mask = (short)-2;
            num = (short)(num & mask);
            i = i + 1;
        }

        return num;
    }

    private short RightShift(short num, short n)
    {
        var i = 0;
        if (num == 0)
        {
            return 0;
        }

        while (i < n)
        {
            num = (short)(num / 2);
            var mask = 32767;
            num = (short)(mask & num);
            i = i + 1;
        }

        return num;
    }

    public string PrintBinaInt(short integer)
    {
        var ptr = 15;
        var s = "";
        while (ptr > 0)
        {
            var l_sh = 15 - ptr;
            s += RightShift(LeftShift(integer, (short)l_sh), 15);
            ptr = ptr - 1;
        }

        s += (RightShift(LeftShift(integer, 15), 15));
        return s;
    }
}