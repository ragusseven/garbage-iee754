namespace c_sharp_impl;

public record struct Ieee754Number
{
    public ushort _a { private get; init; }
    public ushort _b { private get; init; }

    public static Ieee754Number GetFromInt16(short integer)
    {
        if (integer == 0)
            return new Ieee754Number();

        var sign = integer < 0;
        var secondInt = integer;
        if (secondInt < 0)
            secondInt = (short)-secondInt;
        var index = 0;
        short b = 0;
        var a = (short)0;
        while (secondInt > 0)
        {
            var divideRes = (short)(secondInt / 2);
            var ostatok = (short)(secondInt - divideRes * 2);

            if (secondInt == 1)
            {
                secondInt = divideRes;
                index++;
                continue;
            }

            secondInt = divideRes;
            if (index < 16)
            {
                b = (short)((ushort)b | ostatok << (index));
            }

            index++;
        }

        var bCopy = b;
        if (index >= 8)
        {
            bCopy = (short)(bCopy >> index - 8);
        }
        else
        {
            bCopy = (short)(bCopy << (8 - index));
        }

        a = (short)(a | bCopy);
        b = (short)(b << 16 - index + 8);
        if (index > 16)
        {
            throw new ApplicationException();
        }

        var exp = (short)(127 + index - 1);

        a = (short)((ushort)a | ((sign ? 1 : 0) << 15));
        a = (short)((ushort)a | exp << 7);

        var number = new Ieee754Number()
        {
            _a = (ushort)a,
            _b = (ushort)b
        };
        return number;
    }

    public string ToFloat()
    {
        var tens = new ushort[] { 10000, 10000, 10000, 10000, 10000 }.Reverse().ToArray();
        var twos = new ushort[] {2, 4, 8, 16, 32, 64, 128, 256 };
        var parts = ToParts();

        ushort drob = 0;
        ushort ptr = 0;
        while (ptr <= 4)
        {
            var number = (ushort)((ushort)(parts.drob << ptr) >> (15));
            if (number == 1)
            {
                drob += (ushort)(tens[ptr] / twos[ptr]);
            }


            ptr += 1;
        }


        return ((parts.sign == 1 ? -1 : 1) * (parts.integer)).ToString() + "." + drob;
    }

    public override string ToString()
    {
        var a = Convert.ToString(_a, 2);
        while (a.Length != 16)
        {
            a = "0" + a;
        }

        var b = Convert.ToString(_b, 2);
        while (b.Length != 16)
        {
            b = "0" + b;
        }

        return ToFloat() + " " + a + ":" + b;
    }

    public (ushort sign, ushort integer, ushort drob) ToParts()
    {
        if (_a == 0 && _b == 0)
            return (0, 0, 0);
        var sign = (ushort)(_a >> 15);
        var a = (ushort)((_a << 1) >> 1);
        var mo = (ushort)((a & 127) | 128);
        var my = _b;
        var exp_0 = ((a << 1) >> 8) & ~256;
        var exp = exp_0 - 127;
        var fl_p_ix = 7 - exp;
        while (fl_p_ix > 0)
        {
            var tmp_m_young = (ushort)mo & 1;
            my = (ushort)(my >> 1);
            my = (ushort)(my | tmp_m_young << 15);
            fl_p_ix -= 1;
            mo = (ushort)(mo >> 1);
        }

        while (fl_p_ix < 0)
        {
            mo = (ushort)(mo << 1);
            var tmp_m_young = (ushort)((ushort)(my << 0) >> (15));
            my = (ushort)(my << 1);
            mo = (ushort)(mo | tmp_m_young);
            fl_p_ix += 1;
        }

        return (sign, mo, my);
    }

    public static Ieee754Number FromParts(ushort sign, ushort integer, ushort drob)
    {
        if (sign == 0 && integer == 0 && drob == 0)
            return new Ieee754Number();
        var exp = 0;
        var t_int = integer;
        while (t_int > 1)
        {
            t_int = (ushort)(t_int >> 1);
            exp++;
        }

        var ptr = 0;
        var t_drob = drob;
        while (t_int < 1)
        {
            t_int = (ushort)(t_int << 1);
            var symbol = t_drob >> 15;
            t_drob = (ushort)(t_drob << 1);
            t_int = (ushort)(t_int | symbol);
            exp -= 1;
        }

        if (exp >= 0)
            integer = (ushort)(integer & ~(1 << exp));
        else
            drob = (ushort)(drob & ~(1 << 16 + exp));

        var n_off = exp - 7;
        while (n_off > 0)
        {
            var tmp_m_young = (ushort)integer & 1;
            drob = (ushort)(drob >> 1);
            drob = (ushort)(drob | tmp_m_young << 15);
            n_off -= 1;
            integer = (ushort)(integer >> 1);
        }

        while (n_off < 0)
        {
            integer = (ushort)(integer << 1);
            var tmp_m_young = (ushort)((ushort)(drob << 0) >> (15));
            drob = (ushort)(drob << 1);
            integer = (ushort)(integer | tmp_m_young);
            n_off += 1;
        }

        integer = (ushort)(integer | sign << 15);
        integer = (ushort)((ushort)integer | (exp + 127) << 7);

        return new Ieee754Number()
        {
            _a = integer,
            _b = drob
        };
    }

    public Ieee754Number Plus(Ieee754Number another)
    {
        var (sign_a, integer_a, drob_a) = ToParts();
        var (sign_b, integer_b, drob_b) = another.ToParts();

        var (shift, i_res, d_res) = SumParts(drob_a, drob_b, integer_a, integer_b, sign_a, sign_b);

        if (shift == -1)
        {
            var (_, n_i_res, n_d_res) = SumParts((ushort)~d_res, 1, (ushort)~i_res, 0, 0, 0);
            return FromParts(1, n_i_res, n_d_res);
        }

        if (sign_a == sign_b)
        {
            return FromParts(sign_a, i_res, d_res);
        }

        return FromParts(0, i_res, d_res);
    }

    private (short shift, ushort i_res, ushort d_res) SumParts(ushort drob_a,
        ushort drob_b,
        ushort integer_a,
        ushort integer_b,
        ushort sign_a,
        ushort sign_b)
    {
        var ptr = 0;
        var shift = 0;
        var drob_res = (ushort)0;
        var integer_res = (ushort)0;
        while (ptr < 32)
        {
            if (ptr < 16)
            {
                var a = (short)(drob_a >> ptr) & 1;
                var b = (short)(drob_b >> ptr) & 1;
                if (sign_a == 1)
                    a = -a;
                if (sign_b == 1)
                    b = -b;
                // 0, 1, -1, 10, -10, 11
                var result = (short)(a + b + shift);
                if (result == 0)
                {
                    ptr++;
                    continue;
                }

                if (result == 1)
                {
                    drob_res = (ushort)(drob_res | 1 << ptr);
                    shift = 0;
                }

                if (result == -1)
                {
                    drob_res = (ushort)(drob_res | 1 << ptr);
                    shift = -1;
                }

                if (result == -2)
                    shift = -1;

                if (result == 2)
                    shift = 1;

                if (result == 3)
                {
                    drob_res = (ushort)(drob_res | 1 << ptr);
                    shift = 1;
                }
            }
            else
            {
                var t_ptr = ptr - 16;
                var a = (short)(integer_a >> t_ptr) & 1;
                var b = (short)(integer_b >> t_ptr) & 1;
                if (sign_a == 1 && sign_b == 0)
                    a = -a;
                if (sign_b == 1 && sign_a == 0)
                    b = -b;
                // 0, 1, -1, 10, -10, 11
                var result = (short)(a + b + shift);
                if (result == 0)
                {
                    ptr++;
                    shift = 0;
                    continue;
                }

                if (result == 1)
                {
                    integer_res = (ushort)(integer_res | 1 << t_ptr);
                    shift = 0;
                }

                if (result == -1)
                {
                    integer_res = (ushort)(integer_res | 1 << t_ptr);
                    shift = -1;
                }

                if (result == -2)
                    shift = -1;

                if (result == 2)
                    shift = 1;

                if (result == 3)
                {
                    integer_res = (ushort)(integer_res | 1 << t_ptr);
                    shift = 1;
                }
            }

            ptr++;
        }

        return ((short)shift, integer_res, drob_res);
    }
}